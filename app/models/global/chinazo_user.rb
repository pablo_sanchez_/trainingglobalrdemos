class Global::ChinazoUser < ActiveRecord::Base
	validates_presence_of :username, :gender, :cedula, :email, :created_by
	scope :active,->{
		where(active: true)
	}
	scope :inactive,->{
		where(active: false)
	}	
	scope :status,->(status){  
		where(active: status)
	}
end
